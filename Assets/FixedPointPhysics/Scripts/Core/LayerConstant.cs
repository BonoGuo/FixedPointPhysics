namespace BlueNoah
{
    public static class LayerConstant
    {
        public const int DefaultLayer = 0;
        public const int WaterSurfaceLayer = 4;
        public const int GroundLayer = 10;
        public const int AllianceOneLayer = 11;
        public const int AllianceTwoLayer = 12;
        public const int DebugLayer = 13;
        public const int AllianceThreeLayer = 14;
        public const int BarrierLayer = 15;
        public const int WallLayer = 16;
        public const int MiddlePropsLayer = 17;
        public const int Boulder = 18;
        public const int AreaPoint = 19;
        public const int TreasureBoxLayer = 20;
        public const int GoalAreaLayer = 21;
        public const int CheckPointAreaLayer = 22;
        public const int GoalPlayerLayer = 23;
    }
}